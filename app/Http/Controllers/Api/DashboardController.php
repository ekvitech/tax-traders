<?php
/**
 * Created by PhpStorm.
 * User: Ekvitech
 * Date: 03-Mar-18
 * Conteoller manages the dashboard application
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //Validator validates for the integer field
        $validator = Validator::make($request->all(), [
            'number' => 'required|integer|min:0',
            'buttonValue' =>  'required|integer'
        ]);

       $number=request()->input('number');
       $buttonVal=request()->input('buttonValue');


    //number validation check and processing
        if(!$validator->fails() && $number !=''  && strlen($number) > 0 )  {
            $sum='0';
                if($buttonVal==1 || $buttonVal==2) { //Code executes only when button 1 & 2 is clicked
                    for ($i = 0; $i < strlen($number); $i++) {
                     if ($buttonVal == 1) { //String check for odd indexed digit numbers
                            if ($number[$i] % 2 != 0) {
                                $sum = $sum + intval($number[$i]);
                                }
                            }
                        else if($buttonVal == 2){ //String check for even indexed digits numbers
                            if ($number[$i] % 2 == 0) {
                                $sum = $sum + intval($number[$i]);
                                }
                            }
                        else{
                                $sum = $sum + intval($number[$i]);
                            }
                    }
                }
            else{ // code executes when button 3 is clicked and sum up all numbers up to the entered number
                for ($i = 1; $i <= $number; $i++)
                        $sum = $sum + $i;
                }
            //stores the calculation and displays in the view part.
            $resultField=$sum;
        }
        else{ // Code executes when validation fails
            $resultField="Please enter a valid positive integer";
        }


        return response()->json($resultField);
    }


}
