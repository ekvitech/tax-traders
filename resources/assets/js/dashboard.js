/**
 * Created by Arvind on 03-Mar-18.
 */
Vue.component('dashboard', {
    props: [''],

    data() {
    return {
        inputField: '',
        button1: 'Button 1',
        button2: 'Button 2',
        button3: 'Button 3',
        resultField: '',
        method:'1',
    }
},

ready() {
    console.log('[dashboard] - ready() :: ');
   // this.getTaxes();
},
events: {},
computed:{
    isPostiveInteger(){  //Validate a positive integer
        return /^\+?(0|[1-9]\d*)$/.test(this.inputField);
    },
},

methods: {


    processButton(val)
    {
        if (this.inputField !== '') {
            if (this.method == 1)
                this.processButtonThroughVue(val)
            else
                this.processButtonThroughPHP(val)
        }
        else
        {
            this.resultField = "Please enter a valid integer";
        }

    },

    processButtonThroughVue(val){  //Process  logic through Vue.js
        console.log('[dashboard] - Perform Processing Through Javascript');
        //Convert the input field into string

        var string=this.inputField.toString();
        //Defined temporary variable to store sum values.
        var sum=0;
        //Check if the sting exists and is positive integer
        if(string.length > 0 && this.isPostiveInteger && !isNaN(string) && parseInt(Number(string)) == string && !isNaN(parseInt(string, 10) ))  {
            if(val==1 || val==2) { //Code executes only when button 1 & 2 is clicked
                for (var i = 0; i < string.length; i++) {
                    if (val == 1) { //String check for odd indexed digit numbers
                        if (string[i] % 2 != 0) {
                            sum = sum + Number(string[i]);
                        }
                    }
                    else if(val == 2){ //String check for even indexed digits numbers
                        if (string[i] % 2 == 0) {
                            sum = sum + Number(string[i]);
                        }
                    }
                    else{
                        sum = sum + Number(string[i]);
                    }
                }
            }
            else{ // code executes when button 3 is clicked and sum up all numbers up to the entered number
                for (var i = 1; i <= string; i++)
                    sum = sum + i;
            }
            //stores the calculation and displays in the view part.
            this.resultField=sum;
        }
        else{ // Code executes when validation fails
            this.resultField="Please enter a valid integer";
        }
    },

    processButtonThroughPHP(val){  // Process executes through PHP script on server side using AJAX request
        console.log('[Dashboard] - Process through PHP');
        this.form={'number':this.inputField,buttonValue:val}
        this.$http.post('/api/dashboard',this.form )
            .then(response => {
            console.log('[Dashboard] - Process through PHP', response);
            this.resultField=response.body;
    }).catch(function(error) {
        console.error(error);
    });
    },
},

});
