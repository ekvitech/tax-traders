@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
<!-- Component Template starts here -->
<dashboard inline-template>
                    <div>
                        <div class="form-group">
                            Select Processing Method : Vue.js <input type="radio" name="method" v-model="method" value="1"> &nbsp; PHP <input type="radio" name="method" v-model="method" value="2">
                        </div>
                        <div class="form-group" >
                            <input type="text" name="inputField" v-model="inputField" value="" >
                            <br/><br/>

                            <strong>Result</strong> :  <span >@{{ resultField }}</span>
                            <br>
                        </div>
                        <div  class="form-group">
                        <br>
                        <input type="button" name="button1" value="Button 1"  class="btn btn-lg"  v-model="button1" v-on:click="processButton(1)" />
                        <input type="button" name="button1"  v-model="button2"  class="btn btn-lg"    value="Button 1" v-on:click="processButton(2)"/>
                        <input type="button" name="button1" class="btn btn-lg" value="Button 1"  v-model="button3" v-on:click="processButton(3)" />
                        </div>
                    </div>
</dashboard>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
